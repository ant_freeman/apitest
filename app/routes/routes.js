module.exports = function(app, db){
	var tableid = 0;
	app.get('/user/:id', (req, res)=>{
		var id = req.params.id;
		var users = db.getCollection('users');
    	var user = users.chain().find({'id':parseInt(id)}).data();
    	if(user.length > 0){
    		res.send(user);
    	}else{
    		res.send('No records found');
    	}
	});
	app.post('/user', (req, res) => {

		var email = req.body.email;
		console.log('email '+email)
		if(email && email != '' && email.indexOf('@')> -1){
			var users = db.getCollection('users');
			var user = users.chain().find({'email':email}).data();
			if(user.length >0){
				res.send('Email Address already exists');
			}else{
				var lastId = users.chain().find().simplesort('id',true).data();
				if(lastId[0]){
					lastId = lastId[0].id+1;
				}else{
					lastId = 0;
				}
				var result = users.insert({id:lastId,email:email, givenName: req.body.givenName, familyName: req.body.familyName, created: new Date()});
				res.send(result);
			}
		}else{
			res.send('Invalid Email Address');
		}
	});
	app.delete('/user/:id', (req, res)=>{
		var id = req.params.id;
		var users = db.getCollection('users');
		var user = users.chain().find({'id':parseInt(id)}).data();
    	if(user.length > 0){
    		users.remove(user);
    		res.send('user '+id+ ' removed')
    	}else{
    		res.send('No records found');
    	}
	});

	app.put('/user/:id', (req, res)=>{
		var id = req.params.id;
		var users = db.getCollection('users');
		var user = users.chain().find({'id':parseInt(id)}).data();
		if(user.length >0){
			if(req.body.email){
				user[0].email = req.body.email;
			}
			if(req.body.givenName){
				user[0].givenName = req.body.givenName;
			}
			if(req.body.familyName){
				user[0].familyName = req.body.familyName;
			}
    		users.update(user);
   
    		res.send(user)
    	}else{
    		res.send('No records found');
    	}
	});
};