// server.js
const express        = require('express');
const bodyParser     = require('body-parser');
const app            = express();
const loki 			 = require('lokijs');

const port = 8000;
const db = new loki('antapi');
db.addCollection('users', { indices: ['id', 'email', 'givenName', 'familyName','created'] });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./app/routes')(app, db);

app.listen(port, () => {
  console.log('We are live on ' + port);
});