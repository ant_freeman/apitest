## Run the API

Installation

1. Install node https://nodejs.org/en/download/
2. Navigate to the project
3. npm install
4. npm run dev

--

Use Postman to send requests to the API  
down load here https://www.getpostman.com/apps

1. INSERT - Ids are created dynamically starting at 0.  
	http://localhost:8000/user  
	In Postman select POST and in the body select raw and select JSON(application/json)  
	{  
		"email": "{email address}",  
	    "givenName": "{first name}",  
	    "familyName": "{surname}"  
	}  

2. GET  
	http://localhost:8000/user/{id}  
	In Postman select GET  
	if the id is valid, the user details are returned  

3. UPDATE  
	http://localhost:8000/user/{id}  
	In Postman select PUT and in the body select raw  and select JSON(application/json)   
	in the body raw enter  
	{  
		"email": "{email address}",  
	    "givenName": "{first name}",  
	    "familyName": "{surname}"  
	}  

4. DELETE  
	http://localhost:8000/user/{id}  
	In Postman select DELETE  
	if the id is valid, the user details are deleted  
